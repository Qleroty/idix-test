<?php
/**
 * Created by PhpStorm.
 * User: TSOUKA
 * Date: 15/01/2019
 * Time: 00:26
 */

namespace App\Entity;


class Form
{
    protected $form;
    protected $dueDate;

    public function getForm() {
        return $this->form;
    }

    public function setForm($form) {
        $this->form = $form;
    }

    public function getDueDate() {
        return $this->dueDate;
    }

    public function setDueDate(\DateTime $dueDate) {
        $this->dueDate = $dueDate;
    }
}