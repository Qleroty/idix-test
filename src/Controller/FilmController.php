<?php
/**
 * Created by PhpStorm.
 * User: TSOUKA
 * Date: 13/01/2019
 * Time: 23:13
 */
namespace App\Controller;

use App\Entity\Character;
use App\Entity\Film;
use App\Entity\Form;
use App\Form\CharacterType;
use App\Form\filmType;
//use Doctrine\DBAL\Types\TextType;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
Use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Yaml\Tests\A;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class FilmController extends Controller{
    /**
     * @Route("/", name="film_list")
     * @Method({"GET"})
     */
    public function index()
    {


        $films= $this->getDoctrine()->getRepository(Film::class)->findAll();
      //  return new Response('HELLOW');
        if (null === $films) {
            throw new NotFoundHttpException("pas de films.");
        }
        return $this->render('films/index.html.twig', array('films' => $films));
    }


    /**
     * @Route("/film/{id}", name="film_show")
     * @param $id
     * @return Response
     */
    public function show($id) {
        $em = $this->getDoctrine()->getManager();

        $film = $em->getRepository(Film::class)->find($id);

        if (null === $film) {
            throw new NotFoundHttpException("Le film ayant l'ID ".$id." n'existe pas.");
        }

        //$em = $this->getDoctrine()->getManager();

        //$characters = $em->getRepository(Character::class)->findAll();


        $em->flush();

        return $this->render('films/show.html.twig', array(
            'film' => $film
        ));
    }



    /**
     * @Route("/newFilm", name="film_new")
     *
     */
    public function new(Request $request) {
        $film = new Film();
        $form = new Form();
        $form->getForm('Write a blog post');
        $form->getDueDate();

        $form = $this->createFormBuilder($film)
            ->add('title', TextType::class)
            ->add('body', TextType::class)
            ->add('date', DateType::class)
            ->add('save', SubmitType::class, array('label' => 'Créer nouveau film'))
            ->getForm();



        //Si POST
        if ($request->getMethod()) {
            //ON fait le lien requete/formulaire
            //de la , la variable film contient les valeurs entrées dans le forumualire par le visiteur
            $form->handleRequest($request);

            if ($form->isSubmitted()) {
                //on check que les entrées sont correctes
                if ($form->isValid()) {
                    //enregistrement de $film dans BDD
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($film);
                    try {
                        $film->setDate(new \DateTime());
                    } catch (\Exception $e) {
                    }

                    $em->flush();
                    //On redirige vers la liste des films
                    return $this->redirectToRoute('film_list');
                }
            }
        }

        return $this->render('films/newFilm.html.twig', array(
            'form' =>$form->createView(

            ),
        ));


    }


    /**
     * @Route("/newchar", name="character_new")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */

    public function newch(Request $request) {
        $character = new Character();
        $formC = new Form();
        $formC->getForm('Write a blog post');
        //$idFilm = $this->getDoctrine()->getRepository(Film::class)->getId();

        $formC = $this->createFormBuilder($character)
            ->add('name', TextType::class)
           /* ->add('film', ChoiceType::class, [
                //choices from this entity
                'choice_value' => Film::class,

                //Film.title property as visible option string
                'choice_label' => function ($film) {
                    return $film->getTitle();
                },

            ])*/
            ->add('save', SubmitType::class, array('label' => 'Ajouter nouveau personnage'))
            ->getForm();



        //Si POST
        if ($request->getMethod()) {
            //ON fait le lien requete/formulaire
            //de la , la variable film contient les valeurs entrées dans le forumualire par le visiteur
            $formC->handleRequest($request);

            if ($formC->isSubmitted()) {
                //on check que les entrées sont correctes
                if ($formC->isValid()) {
                    //enregistrement de $character dans BDD
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($character);

                    $em->flush();
                    //On redirige vers la liste des films
                    return $this->redirectToRoute('film_list');
                }
            }
        }

        return $this->render('films/newchar.html.twig', array(
            'formC' =>$formC->createView(),
        ));


    }


    /**
     *
     * @Route("/film/save", name="film_save")
     */

    public function save() {
        $entityManager = $this->getDoctrine()->getManager();

        $film = new Film();
        $film->setTitle('Film One');
        $film->setBody('This is the body for film one');

        $entityManager->persist($film);

        $entityManager->flush();

        return new Response('Saves an film with the id of '.$film->getId());
    }





}
